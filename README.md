# SplitPather

This script search in a source folder files with an extension and copy them to other destination path with a diferent structure where the name of the folder is first level is the first letter of the file, second one is the second, etc... to get easy to find files in a gotek or cpcduino by example.

I add normalization to avoid FAT32 problems (all files and directories in lowercase, change spaces to _, make destination path only with name and letter of file name).

For example: we have a file called 3D Grand Prix.DSK, this will copied in destination path (using default 3 levels):3/d/g/3d_gran_prix.dsk
We avoid problems with double files.

Syntax:
======
splitpather START_PATH DESTINATION_PATH [EXT] [DEPTH]
First two parameters are mandatory but you can define the extension of the files and the depth.
I suppose than 3 levels is good for Amstrad DSKs but too much for console ROMs. 

This is very useful to explore a big number of files in devices like goteks, cpcduino, tzxduino, tapuino, casduino, cs2sd, etc...

You can download [the lastest version of the script here](https://gitlab.com/cpcbegin/splitpather/-/archive/master/splitpather-master.zip).

More info about the script on Spanish in [this section of the blog 'Malagueños Originales y Libres'](https://malagaoriginal.blogspot.com/search/label/programaci%C3%B3n+gnu+linux)
